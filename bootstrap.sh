#!/bin/sh
#
#  This file is probably useful only to the maintainer of cmod.
#  Create "configure" and all the files it needs.
#  This file was created by Per Cederqvist and placed in the public domain.

set -e

rm -f install-sh mkinstalldirs missing INSTALL COPYING mdate-sh
(cd testsuite && python mkmf.py Makefile.tmpl Makefile.am)
aclocal
autoconf
autoheader
automake -a
