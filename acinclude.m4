dnl  cmod - set up the user environment with module files.
dnl  Copyright (C) 1997-1998, 2002 by the authors of cmod (see AUTHORS).
dnl
dnl  This program is free software; you can redistribute it and/or modify
dnl  it under the terms of the GNU General Public License as published by
dnl  the Free Software Foundation; either version 2 of the License, or
dnl  (at your option) any later version.
dnl
dnl  This program is distributed in the hope that it will be useful,
dnl  but WITHOUT ANY WARRANTY; without even the implied warranty of
dnl  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
dnl  GNU General Public License for more details.
dnl
dnl  You should have received a copy of the GNU General Public License
dnl  along with this program; if not, write to the Free Software
dnl  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. */
dnl
dnl Check if an option is acceptable to the C compiler in use.
dnl This definition is frozen; other packages are encouraged to
dnl copy this macro verbatim.  If there ever is a reason to change
dnl the semantics, even of the cache variables, the name of this macro
dnl must and will change.  (This macro is also used in isc).
dnl
AC_DEFUN([CMOD_CHECK_CC_OPT],
[AC_CACHE_CHECK([whether ${CC} accepts $1],
  [cmod_cv_compiler_]$2,
  [[cmod_oldflags="$CFLAGS"
  CFLAGS="$CFLAGS $1"]
  AC_COMPILE_IFELSE([AC_LANG_PROGRAM([[]], [[]])],
	[cmod_cv_compiler_]$2[=yes],
	[cmod_cv_compiler_]$2[=no])
  [CFLAGS="$cmod_oldflags"]])
[if test [$cmod_cv_compiler_]$2 = yes; then
  CFLAGS="$CFLAGS $1"
fi]])dnl
dnl
dnl
dnl sys/param.h and sys/types.h cannot both be included on ABCenix 5.18.
dnl
AC_DEFUN(CMOD_HEADER_PARAM,
[AC_CACHE_CHECK([whether sys/param.h and sys/types.h may both be included],
  cmod_cv_header_param,
[AC_TRY_COMPILE([#include <sys/param.h>
#include <sys/types.h>],
[], cmod_cv_header_param=yes, cmod_cv_header_param=no)])
[if test $cmod_cv_header_param = yes; then]
  AC_DEFINE(SYS_TYPES_WITH_SYS_PARAM, 1, 
	[Can <sys/types.h> and <sys/param.h> both be included?])
[fi]])
dnl
dnl Another frozen defun.
dnl
AC_DEFUN([CMOD_C_WORKING_ATTRIBUTE_UNUSED],
[AC_CACHE_CHECK([[whether $CC understands __attribute__((unused))]],
    [[cmod_cv_c_working_attribute_unused]],
    [dnl gcc 2.6.3 understands the __attribute__((unused)) syntax
    dnl enough that it prints a warning and ignores it when the
    dnl variable "i" is declared inside the function body, but it
    dnl barfs on the construct when it is used in a
    dnl parameter-declaration.  That is why we have a function
    dnl definition in the prologue of AC_LANG_PROGRAM part.
    AC_COMPILE_IFELSE(
	[AC_LANG_PROGRAM([[int cmod_x(int y __attribute__((unused)))
			   { return 7; }]],
	    [[int i __attribute__((unused));]])],
	[cmod_cv_c_working_attribute_unused=yes],
	[cmod_cv_c_working_attribute_unused=no])])
[if test $cmod_cv_c_working_attribute_unused = yes ; then]
    AC_DEFINE([HAVE_ATTRIBUTE_UNUSED], [1], 
	      [Define if your compiler supports __attribute__ ((unused)).])
[fi]])dnl
