#!/bin/bash
# $Id$
# Copyright (C) 1995-1996, 1999, 2002  Lysator Academic Computer Association.
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# Please mail bug reports to bug-lyskom@lysator.liu.se.

if [ ! -f cmod_pp.c ]; then
    echo $0: must be invoked from base of cmod source >&2; exit 1
fi
if [ ! -f CVS/Entries ]; then
    echo $0: must be invoked in a CVS working copy of the sources >&2; exit 1
fi
find . \( -type f -print \) -o \( -name CVS -prune \) \
	| sed \
		-e '/#/d' \
		-e '/\.deps\//d' \
		-e '/\.o$/d' \
		-e '/\.orig$/d' \
		-e '/\.rej$/d' \
		-e '/\.tar\.gz$/d' \
		-e '/\.tmp$/d' \
		-e '/\/.cvsignore$/d' \
		-e '/\/ChangeLog/d' \
		-e '/\/Makefile$/d' \
		-e '/\/Makefile.in$/d' \
		-e '/\/README$/d' \
		-e '/\/TAGS$/d' \
		-e '/\/autom4te.cache\//d' \
		-e '/^\.\/AUTHORS$/d' \
		-e '/^\.\/COPYING$/d' \
		-e '/^\.\/MACHINES$/d' \
		-e '/^\.\/NEWS$/d' \
		-e '/^\.\/README\.DEVO$/d' \
		-e '/^\.\/RELEASING$/d' \
		-e '/^\.\/THANKS$/d' \
		-e '/^\.\/TODO$/d' \
		-e '/^\.\/UNDESIGNED$/d' \
		-e '/^\.\/aclocal\.m4/d' \
		-e '/^\.\/bash\.in$/d' \
		-e '/^\.\/bash\.init$/d' \
		-e '/^\.\/bootstrap\.sh$/d' \
		-e '/^\.\/cmod$/d' \
		-e '/^\.\/cmod-copyrights$/d' \
		-e '/^\.\/cmod\.info/d' \
		-e '/^\.\/cmod_strn\.h$/d' \
		-e '/^\.\/config.h$/d' \
		-e '/^\.\/config\.cache$/d' \
		-e '/^\.\/config\.h\.in/d' \
		-e '/^\.\/config\.log$/d' \
		-e '/^\.\/config\.status$/d' \
		-e '/^\.\/configure$/d' \
		-e '/^\.\/csh\.in$/d' \
		-e '/^\.\/csh\.init$/d' \
		-e '/^\.\/enforce-recheck$/d' \
		-e '/^\.\/ksh\.in$/d' \
		-e '/^\.\/ksh\.init$/d' \
		-e '/^\.\/mkinstalldirs$/d' \
		-e '/^\.\/mkrel\.sh$/d' \
		-e '/^\.\/path\.csh$/d' \
		-e '/^\.\/path\.csh.in$/d' \
		-e '/^\.\/path\.sh$/d' \
		-e '/^\.\/path\.sh\.in$/d' \
		-e '/^\.\/sh\.in$/d' \
		-e '/^\.\/sh\.init$/d' \
		-e '/^\.\/stamp-depend$/d' \
		-e '/^\.\/stamp-h$/d' \
		-e '/^\.\/stamp-h1$/d' \
		-e '/^\.\/stamp-h\.in/d' \
		-e '/^\.\/stamp-vti$/d' \
		-e '/^\.\/tcsh\.in$/d' \
		-e '/^\.\/tcsh\.init$/d' \
		-e '/^\.\/testsuite\/clean-sh$/d' \
		-e '/^\.\/testsuite\/t[0-9]*-/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.ok$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.ok\.gen$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.ok\.in$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.out[sg][sg]$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.sh$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.sh\.gen$/d' \
		-e '/^\.\/testsuite\/t[0-9]*\.sh\.in$/d' \
		-e '/^\.\/update-copyright$/d' \
		-e '/^\.\/version\.texi$/d' \
		-e '/^\.\/zsh\.in$/d' \
		-e '/^\.\/zsh\.init$/d' \
		-e '/~$/d' \
	| ./update-copyright
